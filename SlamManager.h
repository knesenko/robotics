/*
 * SlamManager.h
 *
 *  Created on: Sep 27, 2013
 *      Author: Kiril Nesenko, Nokky Goren, Tal Golan and Victor Mesika 
 */

#ifndef SLAMMANAGER_H_
#define SLAMMANAGER_H_

#include "Robot.h"
#include "Mapping/Particle.h"
#include "Mapping/Map.h"
#include "ParticleList.h"
#include <iostream>
#include <queue>


class SlamManager {
public:
	SlamManager(Robot* r);
	virtual ~SlamManager();
	void SlamUpdate();
	void FastUpdate();
	void DrawMap();
private:
	Particle* p;
	Particle* maxParticle;
	ParticleList* pList;
	Robot* _robot;
	int picNum;

	priority_queue<Particle*,vector<Particle*>,CompareParticles> pq;
};

#endif /* SLAMMANAGER_H_ */
