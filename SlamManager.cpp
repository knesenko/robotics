/*
 * SlamManager.cpp
 *
 *  Created on: Sep 27, 2013
 *      Author: Kiril Nesenko, Nokky Goren, Tal Golan and Victor Mesika 
 */

#include "SlamManager.h"
#include "pngwriter.h"
#include "ParticleList.h"
#include <iostream>
#include <queue>

#include <sstream>  // for check
#include <string.h> // for check

using namespace std;


SlamManager::SlamManager(Robot* robot) {
	_robot = robot;
	Particle* p = new Particle(robot->getSnapshot());
	pq.push(p);
	picNum = 0;
}

// this function takes first the higher p.bel and check them
// if reach to max size of queue before checking all particles - so remove them and not update them
void SlamManager::FastUpdate() {
	cout << "Start FastUpdate: current size of pq: " << pq.size() << endl;
	RobotSnapshot* rs = _robot->getSnapshot();
	priority_queue<Particle*,vector<Particle*>,CompareParticles> tmpPq;
	// while the current pq is not empty
	while(!pq.empty()) {
		Particle* p = pq.top();

		// first check the size limit
		if(tmpPq.size() >= (unsigned)MAX_PARTICLES) {
			delete p;
			pq.pop();
			continue;
		}

		// size limit is ok can go over the paritcle:
		// for each particle - update it
		p->updateP(rs);

		cout << "particle "<< p <<" just updated. current bel is " << p->getBel() << endl;
		// ABOVE HIGH TH
		if(p->getBel() > DUP_THRESHOLD) {

			// check if enough space for duplicated particles, else insert as many as you can
			int fixedNumOfDuplicates = DUP_PARTICLES;
			if((MAX_PARTICLES - tmpPq.size()) < (unsigned)DUP_PARTICLES) {
				fixedNumOfDuplicates = int(MAX_PARTICLES - tmpPq.size());
			}

			// duplicating the new particles according this one
			for(int i=0;i<fixedNumOfDuplicates;i++) {
				tmpPq.push(new Particle(p));
			}
		}

		// BELOW LOW TH
		if(p->getBel() < DELETE_THRESHOLD) {
			// is not going to delete the particle if the new pq is empty
			if(!tmpPq.empty()) { delete p; } else { tmpPq.push(p); }
		} else { // ABOVE LOW TH - keep particle and push the new pq
			tmpPq.push(p);
		}

		pq.pop();
	}

	// the new pq as the main one
	pq = tmpPq;
	DrawMap();
	delete rs;
}

void SlamManager::SlamUpdate() {
	bool done = false;
	Particle* p1;
	while(pList->hasNext()){
		p1 = pList->getNext();
		p1->updateP(_robot->getSnapshot());
		if(p1->getBel() < DELETE_THRESHOLD) {
			pList->remove();
			continue;
		}
	}
	maxParticle = pList->getHead();
	int currentCount = pList->getCount();
	while(currentCount < 100
			|| done == true) {
		if(pList->hasNext()) {
			p1 = pList->getNext();
			for(int i=0; i<3; i++) {
				if(currentCount < 100) {
					Particle* p2 = new Particle(p1);
					pList->add(p2);
					currentCount++;
				}else {
					done = true;
					break;
				}
			}
		}
	}
}

void SlamManager::DrawMap() { // testing
	picNum++;
	stringstream strs;
	strs << "/home/user/MAP/PrintNumber";
	strs << picNum;
	string temp_str = strs.str();
	char* char_type = (char*) temp_str.c_str();
	pngwriter png(MAP_SIZE,MAP_SIZE,1.0,char_type);
	Particle* maxP = pq.top();
	Map* printMap = maxP->getMap();
	for(int i=0; i<MAP_SIZE; i++) {
		for(int j=0; j<MAP_SIZE; j++){
			int drawNum = printMap->getPosition(i,j);
			int x = i;
			int y = j;
			if(drawNum == MAP_CELL_FILLED) {
				png.plot(x,y,0.0,0.0,0.0);
			}
			if(drawNum == MAP_CELL_UNKNOWN) {
				png.plot(x,y,1.0,0.0,0.0);
			}
			if(drawNum == MAP_CELL_FREE) {
				png.plot(x,y,1.0,1.0,1.0);
			}
		}
	}
	Point* p = printMap->worldToMap(maxP->getX(),maxP->getY());
	png.plot(p->x,p->y,0.0,0.0,1.0);
	for(int i=0;i<5;i++) {
		png.circle(p->x,p->y,i,0.0,0.0,1.0);
	}

	png.close();
}

SlamManager::~SlamManager() {
	delete p;
	delete pList;
}
