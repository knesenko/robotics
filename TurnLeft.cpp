/*
 * TurnLeft.cpp
 *
 *  Created on: Aug 9, 2013
 *      Author: Kiril Nesenko, Nokky Goren, Tal Golan and Victor Mesika
 */

#include "TurnLeft.h"
#include "Basedefs.h"

int TurnLeft::_generateRandom()
{
	srand(time(NULL));
	return rand() % 2 + 1;
}

bool TurnLeft::startCond()
{
	if(_generateRandom() == 1)
	{
		return false;
	}

	int _start = LEFT_START_RAY;
	int _end = LEFT_END_RAY;
	int _counter = 0;
	double _scan = 0.0;
	bool _flag = false;

	for(int i=_start;i<_end;i++)
	{
		_scan = _robot->getScan(i);
		if(_scan >= LEFT_BLOCKED_DISTANCE)
		{
			_flag = true;
			_counter++;
			cout << "counter: " << _counter << endl;
			if(_flag == true
					&& _counter >= (LEFT_END_RAY - LEFT_START_RAY) - 260)
			{
				laserToStop = i-10;
				YawToStop = _robot->yawToRadians(laserToStop);
				return true;
			}
		} else {
			_flag = false;
			_counter = 0;
		}

	}
	return false;
}

void TurnLeft::action()
{
	_robot->setSpeed(0.0, LEFT_SPEED);
}

bool TurnLeft::stopCond()
{
	double _scan = 0.0;
	double _currentYaw = 0.0;
	double _delta_yaw_plus = 0.0;
	double _delta_yaw_minus = 0.0;

	while(true)
	{
		_currentYaw = _robot->getYaw();

		if(YawToStop < 0.0)
		{
			_delta_yaw_plus = YawToStop - 0.05;
			_delta_yaw_minus = YawToStop + 0.05;
		} else {
			_delta_yaw_plus = YawToStop + 0.05;
			_delta_yaw_minus = YawToStop - 0.05;
		}

		if(_currentYaw < 0)
		{
			if(_currentYaw >= _delta_yaw_plus
					&& _currentYaw <= _delta_yaw_minus)
			{
				_robot->setSpeed(0.0, 0.0);
				return true;
			}
		}
		else
		{
			if((_currentYaw <= _delta_yaw_plus)
					&& (_currentYaw >= _delta_yaw_minus))
			{
				_robot->setSpeed(0.0, 0.0);
				return true;
			}
		}

		return false;
	}
}

TurnLeft::~TurnLeft() {
	// TODO Auto-generated destructor stub
}
