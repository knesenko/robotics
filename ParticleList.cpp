/*
 * ParticleList.cpp
 *
 *  Created on: Sep 29, 2013
 *      Author: Kiril Nesenko, Nokky Goren, Tal Golan and Victor Mesika
 */

#include "ParticleList.h"
#include "Mapping/Particle.h"
#include "Node.h"
#include <list>
#include <cctype>
#include <iostream>

using namespace std;

void ParticleList::add(Particle *p)
{
	Node* _curr = _head;
	Node* _prev = _head;
	Node* _node = new Node(p);
	_size++;
	while(_curr->_next) {
		if(_curr->_particle->getBel() >= p->getBel()) {
			_prev = _curr;
			_curr = _curr->_next;
			continue;
		}
		_prev->_next = _node;
		_node->_next = _curr;
		return;
	}
	//if there is no node after, insert last, and move tail pointer to inserted node.
	_tail->_next = _node;
	_tail = _node;
}

bool ParticleList::hasNext()
{
	if(_current->_next)
	{
		return true;
	}
	return false;
}

Particle* ParticleList::getHead()
{
	return _head->_next->_particle;
}

int ParticleList::getCount()
{
	return _size;
}

Particle* ParticleList::getNext()
{
	if(hasNext())
	{
		_before_current = _current;
		_current = _current->_next;
		return _current->_particle;
	}
	_before_current = _head;
	_current = _head->_next;
	return NULL;
}

void ParticleList::remove()
{
	if(hasNext())
	{
		_before_current->_next = &(*_current->_next);
		delete _current->_particle;
		delete _current;

		_current = _current->_next;
		_size--;
		return;
	}

	_current = _head->_next;
	_tail = _before_current;
	_before_current = _head;
}
