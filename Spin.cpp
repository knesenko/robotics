/*
 * Spin.cpp
 *
 *  Created on: Aug 17, 2013
 *      Author: Kiril Nesenko, Nokky Goren, Tal Golan and Victor Mesika
 */
#include "Spin.h"
#include "Basedefs.h"

bool Spin::startCond()
{
	return true;
}

void Spin::action()
{
	_robot->setSpeed(0.0, RIGHT_SPEED);

}
bool Spin::stopCond()
{
	while(true)
	{
		if(_robot->ifForward() == true)
		{
			_robot->setSpeed(0.0, 0.0);
			return true;
		}
		return false;
	}
}

Spin::~Spin() {
	// TODO Auto-generated destructor stub
}

