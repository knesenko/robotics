/*
 * Robot.cpp
 *
 *  Created on: Aug 9, 2013
 *      Author: Kiril Nesenko, Nokky Goren, Tal Golan and Victor Mesika
 */

#include "Robot.h"

void Robot::printPosition() {
	cout << "X: "<< getX()<<", Y: "<<getY()<<", YAW: "<<getYaw() << endl;
}

double Robot::getAngleByID(int i) {
    return double(((i*(240.0/_lp.GetCount()))-120));
}

int Robot::getIdByAngle(double angle) {
	if(angle >= -120 and angle<=120) {
		return ((angle+120)/(240.0/_lp.GetCount()));
	} else {
		return -1;
	}
}

int Robot::getTotalScan() {
	return _lp.GetCount();
}


void Robot::updateRndDeltas() {
	rnd_delYaw = ((rand()%int(M_PI*10000/8))/10000.0)-(M_PI/16); // angle change between -M_PI/16 to M_PI/16
	rnd_delX = (rand()%21 -10)/100.0; // cm between -10 to +10 to M
	rnd_delY = (rand()%21 -10)/100.0; // cm between -10 to +10 to M
}

double Robot::getYaw(){ return _pp.GetYaw() + rnd_delYaw; }
double Robot::getX(){ return _pp.GetXPos() + rnd_delX; }
double Robot::getY(){ return _pp.GetYPos() +rnd_delY; }

RobotSnapshot* Robot::getSnapshot() {
	// this function calculates the delta between current "real" robot position to old snapshot
	// in order to calculate the exactly expected delta
	RobotSnapshot* rs = new RobotSnapshot();
	for(int j=0;j<10;j++) {
		Read();
	}

	rs->delX = _pp.GetXPos()-_lastRs->x;
	rs->delY = _pp.GetYPos()-_lastRs->y;
	//cout << " last X: " << _lastRs->x << "; current X:" << _pp.GetXPos() << "; DELTA: " << rs->delX << endl;

	// delta yaw
	double rbtYaw = _pp.GetYaw();
	double _yaw = _lastRs->yaw;
	//cout << "yaw before: " << _yaw << "; yaw now: " << rbtYaw << ";";
	if(_yaw*rbtYaw >= 0) {
		rs->delYaw = rbtYaw-_yaw;
	} else { // one positive one negative
		rs->delYaw = abs(_yaw)+abs(rbtYaw);
		if(rs->delYaw > M_PI) {
			rs->delYaw = (2.0*M_PI)-rs->delYaw;
		}
		if(_yaw > 0) {
			rs->delYaw = (_yaw <= (M_PI/2) and _yaw > 0 ) ? rs->delYaw*(-1) : rs->delYaw;
		} else {
			rs->delYaw = (_yaw >= -(M_PI/2) and _yaw < 0 ) ? rs->delYaw : rs->delYaw*(-1);
		}
	}
	//rs->delYaw = rs->delYaw*(-1);
	//rs->delYaw = atan2(rs->delY,rs->delX);
	//cout << " deltaYaw is : " << rs->delYaw << endl;
	// update current robot position
	rs->x = getX();
	rs->y = getY();
	rs->yaw = getYaw();
	//

	// update the last RobotSnapshot to be current exactly position
	_lastRs->x = getX();
	_lastRs->y = getY();
	_lastRs->yaw = getYaw();
	//

	double* tmpLp = new double[NO_OF_PROBES];
	for(int i=0;i<NO_OF_PROBES;i++) {
		tmpLp[i] = getScan(i);
	}
	rs->lp = tmpLp;

	return rs;
}

bool Robot::ifForward()
{
	for(int i=FORWARD_START_RAY; i<FORWARD_END_RAY;i++)
	{
		if(getScan(i) < FORWARD_BLOCKED_DISTANCE)
		{
				return false;
		}
	}

	if (getScan(280) < 0.7
			|| getScan(390) < 0.7)
	{
		return false;
	}
	return true;
}

void Robot::Read()
{
	_pc.Read();
}

void Robot::setSpeed(double speed, double angular)
{
	_pp.SetSpeed(speed,angular);
}

double Robot::getScan(int index){return _lp[index];}

double Robot::yawToRadians(int laserSensor)
{
	// convert laser sensor ID to yaw in radians
	int offset = laserSensor - 333;
	if (_pp.GetYaw() + (offset * radsPerProbe) < - M_PI)
	{
		return _pp.GetYaw() + (offset * radsPerProbe) + 2.0 * M_PI;
	}
	else if (_pp.GetYaw() + (offset * radsPerProbe) > M_PI)
	{
		return _pp.GetYaw() + (offset * radsPerProbe) - 2.0 * M_PI;
	}
	else
		return _pp.GetYaw() + (offset * radsPerProbe);
}



Robot::~Robot() {
	// TODO Auto-generated destructor stub
}
