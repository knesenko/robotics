/*
 * main.cpp
 *
 *  Created on: Aug 9, 2013
 *      Author: Kiril Nesenko, Nokky Goren, Tal Golan and Victor Mesika
 */
#include "Manager.h"

int main()
{
	Manager m("localhost",6665);
	m.run();

}
