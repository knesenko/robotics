/*
 * Spin.h
 *
 *  Created on: Aug 17, 2013
 *      Author: Kiril Nesenko, Nokky Goren, Tal Golan and Victor Mesika
 */

#ifndef SPIN_H_
#define SPIN_H_

#include "Behavior.h"

class Spin: public Behavior {
public:
	Spin(Robot* robot):Behavior(robot){}
	void action();
	bool startCond();
	bool stopCond();
	virtual ~Spin();
};

#endif /* SPIN_H_ */
