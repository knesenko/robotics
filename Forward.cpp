/*
 * Forward.cpp
 *
 *  Created on: Aug 9, 2013
 *      Author: Kiril Nesenko, Nokky Goren, Tal Golan and Victor Mesika
 */

#include "Forward.h"
#include "Basedefs.h"

bool Forward::startCond()
{
	start = clock();
	return true;
}

void Forward::action()
{
	_robot->setSpeed(FORWARD_SPEED, 0.0);
}

bool Forward::stopCond()
{
	end = 3*1000+start;
	if(clock() > end)// comment this for loop if robot doesn't goes forward
	{
		_robot->setSpeed(0.0, 0.0);
		return true;
	}

	if(!_robot->ifForward())
	{
		_robot->setSpeed(0.0, 0.0);
		return true;
	}

	_robot->setSpeed(FORWARD_SPEED, 0.0);
	return false;
}

Forward::~Forward() {
	// TODO Auto-generated destructor stub
}
