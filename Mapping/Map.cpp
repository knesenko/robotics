#define _USE_MATH_DEFINES

#include <cmath>
#include "Map.h"
#include "myVector.h"
#include <iostream>
#include <list>
using namespace std;

Map::Map() : _center() {
	int **temp = new int*[MAP_SIZE];
	for(int i=0; i<MAP_SIZE; i++) {
		temp[i] = new int[MAP_SIZE];
		for(int j=0; j<MAP_SIZE; j++) {
			temp[i][j] = MAP_CELL_UNKNOWN;
		}
	}
	_matrix = temp;
	_center = new Point();
	_center->x = (MAP_SIZE-1)/2;
	_center->y = (MAP_SIZE-1)/2;

}

Map::~Map() {
	for(int i=0; i<MAP_SIZE; i++) {
		delete[] _matrix[i];
	}
	delete[] _matrix;
	delete _center;
}

bool Map::isInRange(int x, int y) {
	if(x < 0 || x >= MAP_SIZE) { return false; }
	if(y < 0 || y >= MAP_SIZE) { return false; }
	return true;
}

bool Map::setPosition(int x, int y, int data) {
	if(isInRange(x,y)) {
		_matrix[x][y] = data;
		return true;
	} else {
		return false;
	}
}

bool Map::setPosition(Point* p, int data) {
	int x = p->x;
	int y = p->y;
	return setPosition(x, y, data);
}

int Map::getPosition(Point* p) {
	int x = p->x;
	int y = p->y;
	if(isInRange(x,y)) {
		return _matrix[x][y];
	} else {
		return NULL;
	}
}

int Map::getPosition(int x,int y) {
	if(isInRange(x,y)) {
		return _matrix[x][y];
	} else {
		return NULL;
	}
}

void Map::print() {
	for(int i=0; i<MAP_SIZE; i++) {
		for(int j=0; j<MAP_SIZE; j++) {
			if(_matrix[i][j] == MAP_CELL_FREE) {
				cout << " ";
			}
			if(_matrix[i][j] == MAP_CELL_FILLED) {
				cout << "1";
			}
			if(_matrix[i][j] == MAP_CELL_UNKNOWN) {
				cout << "?";
			}
		}
		cout << endl;
	}
}

Point* Map::worldToMap(double x, double y) {
	Point* tmp = new Point();
	tmp->x = (x*100)/MAP_RES + _center->x;
	tmp->y = _center->y - ((y*100)/MAP_RES);
	return tmp;
}

void Map::fillVector(double angle,double distance,Point* start) {
	//angle = angle - (M_PI/6);
	distance = distance*100/MAP_RES; // set to resolution
	myVector* v = new myVector(start,angle,distance);

	list<Point> lst = *(v->getVectorPoints());
	list<Point>::iterator it=lst.begin();

	// first fill the clean cells
	for(int i=1;i<(int)lst.size();i++) {
		setPosition(&(*it), MAP_CELL_FREE);
		it++;
	}
	// last one is the vector's arrow
	setPosition(&(*it), MAP_CELL_FILLED);
}

// fill vector on map and returns % of collisions out of total vector points
// GET: distance - in real world. Point = in Map position
double Map::fillVectorWithCollisions(double angle,double distance,Point* start) {
	//angle = angle - (M_PI/6);
	bool isFreeVector = distance > 3.99;
	distance = distance*100/MAP_RES; // set to resolution
	myVector* v = new myVector(start,angle,distance);

	int totalCollisions = 0;

	list<Point> lst = *(v->getVectorPoints());
	list<Point>::iterator it=lst.begin();

	int j=1;
	for(it=lst.begin();it!=lst.end();++it) {
		if(!isInRange(it->x,it->y)) {
			j++;
			continue;
		}
		// handle with last point = the ARROW
		if(lst.size()==(unsigned)j && !isFreeVector) {

			// handle with collision
			if(getPosition(&(*it))==MAP_CELL_FREE) {
				totalCollisions++;
			}
			setPosition(&(*it),MAP_CELL_FILLED);
		}
		// else - handle with all other points (including free vector so head as normal point
		else {
			// handle with collision
			if(getPosition(&(*it))==MAP_CELL_FILLED) {
				totalCollisions++;
			}
			setPosition(&(*it),MAP_CELL_FREE);
		}
		j++;
	}

	return double(totalCollisions/lst.size());
//	// first fill the clean cells
//	for(int i=0;i<lst.size();i++) {
//
//		// check if exist cell is clean or not
//		if(getPosition(&(*it))==MAP_CELL_FREE) {
//			// then ok
//		}
//
//		if(getPosition(&(*it))==MAP_CELL_UNKNOWN) {
//			// then ok
//		}
//
//		if(getPosition(&(*it))==MAP_CELL_FILLED) {
//			totalCollisions++;
//		}
//
//		setPosition(&(*it),MAP_CELL_FREE);
//		it++;
//	}
//	// last one is the vector's arrow
//	setPosition(&(*it),MAP_CELL_FILLED);
}


Map* Map::makeCloneMap() {
	Map* m = new Map();
	for(int i=0; i<MAP_SIZE; i++) {
		for(int j=0; j<MAP_SIZE; j++) {
				m->setPosition(i,j,_matrix[i][j]);
		}
	}
	return m;
}

