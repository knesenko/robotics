/*
 * Particle.h
 *
 *  Created on: Aug 26, 2013
 *      Author: Kiril Nesenko, Nokky Goren, Tal Golan and Victor Mesika
 */

#include "Map.h"
#include "../Robot.h"

#ifndef PARTICLE_H_
#define PARTICLE_H_

class Particle {
public:
	Particle(RobotSnapshot* rs);
	Particle(Particle* old);
	virtual ~Particle();
	void updateP(RobotSnapshot* rs);
	void deleteP();
	Particle** createP(int i);
	double getBel();
	double getX();
	double getY();
	double getYaw();
	Map* getMap();

private:
	Map* _map;
	//Robot* _robot;
	double _x;
	double _y;
	double _yaw;
	double _bel;

	double probByMove(double delX,double delY,double delYaw);
	double probByLaser(double* lp);

};

class CompareParticles {
public:
	// operator() overload for p1 < p2 ? by using getBel() value
	bool operator()(Particle* p1, Particle* p2) {
		return p1->getBel() < p2->getBel();
	}
};

#endif /* PARTICLE_H_ */
