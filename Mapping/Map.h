/*
 * Map.h
 *
 *  Created on: Aug 7, 2013
 *      Author: Kiril Nesenko, Nokky Goren, Tal Golan and Victor Mesika
 */

#include "../Basedefs.h"
#ifndef MAP_H_
#define MAP_H_


struct Point {
	int x, y;
};


class Map {
public:
	Map();
	~Map();
	bool setPosition(Point* p, int data);
	bool setPosition(int x, int y, int data);
	int getPosition(Point* p);
	int getPosition(int x,int y);
	void print();
	Point* worldToMap(double x, double y);
	void fillVector(double angle,double distance,Point* start);
	double fillVectorWithCollisions(double angle,double distance,Point* start);
	Map* makeCloneMap();
private:
	int** _matrix;
	Point* _center;
	bool isInRange(int x, int y);
};

#endif /* MAP_H_ */
