/*
 * Particle.cpp
 *
 *  Created on: Aug 26, 2013
 *      Author: Kiril Nesenko, Nokky Goren, Tal Golan and Victor Mesika
 */


#define N_FACTOR 1.15

#include <cmath>
#include "Particle.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

#define _USE_MATH_DEFINES

// this function gets yaw and set it to be in Robot's yaw range (-M_PI to M_PI)
/**double setYawInRange(double yaw) {
	if(yaw >= -M_PI and yaw <= M_PI) { return yaw; }
	// remove cycles
	//int numOf2PIs = //yaw / (2*M_PI);
	//yaw = yaw - (numOf2PIs*2*M_PI);
	int sign = yaw*(-1);
	yaw = (sign>0) ? -fmod(-yaw,2.0*M_PI): fmod(yaw,2.0*M_PI);
	double dif = yaw - M_PI;
	//
	if(yaw >= M_PI) { return -M_PI + dif; }
	if(yaw <= -M_PI) { return M_PI - dif; }
	return yaw;

	if(yaw >= 0 and yaw <= 2.0*M_PI) { return yaw; }

}
**/
double unwindRadians( double radians )
{
   const bool radiansNeedUnwinding = radians < 0 || 2.0*M_PI <= radians;
   if ( radiansNeedUnwinding )
   {
      if ( signbit( radians ) )
      {
         radians = -fmodf( -radians, 2.f * M_PI );
      }
      else
      {
         radians = fmodf( radians, 2.f * M_PI );
      }
   }

   return radians;
}


Particle::Particle(Particle* old) {
	//srand(time(NULL));
	double rnd_delX = (rand()%41 -20)/100.0; // cm between -10 to +10
	double rnd_delY = (rand()%41 -20)/100.0; // cm between -10 to +10
	double rnd_delYaw = ((rand()%int(M_PI*10000/16))/10000.0)-(M_PI/32); // angle change between -M_PI/16 to M_PI/16
	//double r = ((double) rand() / (RAND_MAX));
	_map = old->_map->makeCloneMap();
	_x = rnd_delX + old->_x;
	_y = rnd_delY + old->_y;
	_yaw = rnd_delYaw + old->_yaw;
	//_yaw = fmod(_yaw,2.0*M_PI);
	//_yaw = setYawInRange(_yaw);
	//cout << "o.p X: " << old->_x <<"; o.p Y: " << old->_y << "; o.p YAW: "<< old->_yaw << endl;
	//cout << "n.p X: " << _x << "; n.p Y: " << _y << "; n.p YAW: " << _yaw << endl;
	//cout << "dif X: " << rnd_delX << "; dif Y: "<< rnd_delY << "; dif YAW: " << rnd_delYaw << endl;
	//cout<< "setYawinrange: " << setYawInRange(_yaw) << endl;
	_bel = old->_bel; // same bel as the old one
	//_robot = old->_robot;
}

Particle::Particle(RobotSnapshot* rs) {
	_map = new Map();
	//_x = r->getX();
	//_y = r->getY();
	//_yaw = r->getYaw();

	_x = rs->x;
	_y = rs->y;
	_yaw = rs->yaw + M_PI; // 0 to 2*PI

	_bel = 1.0;
	//_robot = r;
}

Particle::~Particle() {
	delete _map;
}

void Particle::updateP(RobotSnapshot* rs) {
	//double rbtX = _robot->getX();
	//double rbtY = _robot->getY();
	//double rbtYaw = _robot->getYaw();

	double rbtX = rs->x;
	double rbtY = rs->y;
	double rbtYaw = rs->yaw;

	double deltaX = (_x-rbtX);
	double deltaY = (_y-rbtY);

	double deltaYaw;
	// check if the yaws has the same sign
	if(_yaw*rbtYaw > 0) {
		deltaYaw = abs(_yaw-rbtYaw);
	} else { // one positive one negative
		deltaYaw = abs(_yaw)+abs(rbtYaw);
		if(deltaYaw > M_PI) {
			deltaYaw = (2*M_PI)-deltaYaw;
		}
	}

	//cout << "particle yaw is: " << _yaw << " and current robot yaw is " << rbtYaw << " diff is = " << deltaYaw << endl;
	//deltaYaw = (_yaw-rbtYaw);//%M_PI; // mod of M_PI (360c)

	double pBel = _bel * probByMove(abs(deltaX),abs(deltaY),abs(deltaYaw));
	//cout << "current X: " << _x << "; delta is " << rs->delX << endl;
	_x = _x + rs->delX;
	//cout << "current Y: " << _y << "; delta is " << rs->delY << endl;
	_y = _y + rs->delY;
	//cout << "current YAW: " << _yaw << "; delta is " << rs->delYaw;
	_yaw = _yaw + rs->delYaw;
	//_yaw = setYawInRange(_yaw);
	//cout << "; fixed new YAW to : " << _yaw << endl;
	//cout << "current X,Y:" << _x << ";" << _y << endl;
	//
	// calculate belief
	_bel = N_FACTOR * probByLaser(rs->lp) * pBel;
	_bel = (_bel > 1) ? 1 : _bel;
	// then set the current X,Y,YAW
	// after bel is updated - set the particle to current robot X,Y,YAW

}

// returns prob between 0 to 1
double Particle::probByMove(double delX,double delY,double delYaw) {

	double deltaDistance = double((delX*delX)+(delY*delY)); // calculate the distance delta
	deltaDistance = sqrt(deltaDistance);

	//cout << "delta distance :" << deltaDistance << ";" ;
	//cout << "delta Yaw :" << delYaw << ";" ;
	double angle_bel = 1; // prob of angel between 0 to 1
	double distance_bel = 0; // prob of distance between 0 to 1


	if(delYaw>0) {
		//angle_bel = 1/delYaw;
		//angle_bel = abs(angle_bel);
		angle_bel = angle_bel - double(delYaw/(4*M_PI));
	}
	//cout << "Angel prob :" << angle_bel << ";" ;

	deltaDistance = deltaDistance/100;
	// check if in range of 100m. else prob stay 0
	if(deltaDistance<100) {
		// reduce each meter as 0.01 from prob
		distance_bel = 1-deltaDistance;
	}
	//cout << "Distance prob :" << distance_bel << ";" ;
	double prob = (angle_bel*0.6) + (distance_bel*0.4); // 70% for angle delta. 30% for distance delta
	//cout << "probByMove is : " << prob << endl;
	return (prob>1) ? 1 : prob;
}

double getRadianByLaserID(int i) {
    return double((((i*(240.0/NO_OF_PROBES))-120))*(M_PI/180.0));
}

// returns the prob by laser - average of prob from all the laser cells
double Particle::probByLaser(double* lp) {
	// update map
	Point* rbt_pos = _map->worldToMap(_x,_y); // robot position in map
	//cout << " world to map . real world = " << _x << ";" << _y << "; in map= " << rbt_pos->x << ";" << rbt_pos->y << endl;
	double fixedAngle; // set the correct angle of each laser based on robot's current yaw
	double probSum = 0; // sum up all return prob from each angle
	//cout << "probByLaser : start angle is "  << _yaw+getRadianByLaserID(0) << "; end is " << _yaw+getRadianByLaserID(NO_OF_PROBES-1)<< endl;
	for(int i=0;i<NO_OF_PROBES;i++) {

		//fixedAngle = setYawInRange(_yaw+getRadianByLaserID(i));
		fixedAngle = unwindRadians(_yaw+getRadianByLaserID(i));
		//cout << "angle " << i << " : " << fixedAngle << ";" << endl;

		probSum = probSum + _map->fillVectorWithCollisions(fixedAngle,lp[i],rbt_pos);
	}
	double laserProb = double(1-(probSum/NO_OF_PROBES));
	//cout << "probSum is "<< probSum << "laser prob: " << laserProb << endl;
	return laserProb;
}

Particle** Particle::createP(int i) {
	Particle** arr = new Particle*[i];
	for(int j=0;j<i;j++) {
		Particle p = new Particle(this);
		arr[j] = &p;
	}
	return arr;
}

double Particle::getBel() { return _bel; }
double Particle::getX() { return _x; }
double Particle::getY() { return _y; }
double Particle::getYaw() { return _yaw; }

Map* Particle::getMap() { return _map; }
