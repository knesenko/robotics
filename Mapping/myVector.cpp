#define _USE_MATH_DEFINES

/*
 * Vector.cpp
 *
 *  Created on: Aug 26, 2013
 *      Author: Kiril Nesenko, Nokky Goren, Tal Golan and Victor Mesika
 */

#include "myVector.h"
#include "Map.h"
#include <iostream>
#include <list>
#include <cmath>
using namespace std;


myVector::myVector(Point* s,double angle,double distance) {
	_ang = angle;
	_dist = distance;
	_s = s;
}

double toRadians(double d) {
	return (d*(M_PI/180));
}

list<Point>* myVector::getVectorPoints() {
	list<Point>* tmp = new list<Point>;
	for(int i=0;i<=_dist; i++) {
		Point* p = new Point();
		p->x = _s->x + cos(_ang)*(i);
		p->y = _s->y + sin(_ang)*(i);
		tmp->push_back(*p);
	}
	return tmp;
}


myVector::~myVector() {}
