#include <iostream>
#include <list>
#include "Map.h"
using namespace std;
/*
 * myVector.h
 *
 *  Created on: Aug 26, 2013
 *      Author: Kiril Nesenko, Nokky Goren, Tal Golan and Victor Mesika
 */

#ifndef MY_VECTOR_H_
#define MY_VECTOR_H_

class myVector {
public:
	myVector(Point* s,double angle,double distance);
	virtual ~myVector();

	Point* _s;
	double _ang;
	double _dist;

	list<Point>* getVectorPoints();
};

#endif /* MY_VECTOR_H_ */
