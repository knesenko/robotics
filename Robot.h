/*
 * Robot.h
 *
 *  Created on: Aug 9, 2013
 *      Author: Kiril Nesenko, Nokky Goren, Tal Golan and Victor Mesika
 */


#include <iostream>
#include <libplayerc++/playerc++.h>
#include <cmath>
#include "Basedefs.h"


#ifndef ROBOT_H_
#define ROBOT_H_

using namespace std;
using namespace PlayerCc;

// The below is the angle in radians covered by each of the laser probes
const double radsPerProbe= LAZ_SPAN / 360 * 2 * M_PI / static_cast<double>(NO_OF_PROBES);

struct RobotSnapshot {
	double x,y,yaw;
	double delX,delY,delYaw;
	double* lp;
	//RobotSnapshot():x(x),y(y),yaw(yaw),delX(delX),delY(delY),delYaw(delYaw) {};
	~RobotSnapshot() { delete[] lp; };
};


class Robot {
	PlayerClient _pc;
	Position2dProxy _pp;
	LaserProxy _lp;
	double rnd_delYaw;
	double rnd_delX;
	double rnd_delY;
	RobotSnapshot* _lastRs;

public:
	Robot(char* ip, int port):_pc(ip,port), _pp(&_pc,0),
	_lp(&_pc,0){
		_lastRs = new RobotSnapshot();
		_lastRs->x = _pp.GetYPos();
		_lastRs->y = _pp.GetXPos();
		_lastRs->yaw = _pp.GetYaw();
	}

	double getScan(int index);
	int getTotalScan();
	bool ifForward();

	void Read();

	void setSpeed(double speed, double angular);

	double yawToRadians(int laserSensor); // convert laser sensor ID to yaw in radians

	double getAngleByID(int i);

	int getIdByAngle(double angle);


	double getYaw();
	double getX();
	double getY();
	void updateRndDeltas();
	RobotSnapshot* getSnapshot();

	void printPosition();

	virtual ~Robot();
};


#endif /* ROBOT_H_ */
