/*
 * TurnRight.cpp
 *
 *  Created on: Aug 9, 2013
 *      Author: Kiril Nesenko, Nokky Goren, Tal Golan and Victor Mesika
 */

#include "TurnRight.h"
#include "Basedefs.h"

bool TurnRight::startCond()
{
	int _start = RIGHT_START_RAY;
	int _end = RIGHT_END_RAY;
	int _counter = 0;
	double _scan = 0.0;
	bool _flag = false;

	for(int i=_end;i>=_start;i--)
	{
		_scan = _robot->getScan(i);

		if(_scan >= RIGHT_BLOCKED_DISTANCE)
		{
			_flag = true;
			_counter++;
			if(_flag == true
					&& _counter >= (RIGHT_END_RAY - RIGHT_START_RAY) - 260)
			{
				laserToStop = i-10;
				YawToStop = _robot->yawToRadians(laserToStop);
				return true;
			}
		} else {
			_flag = false;
			_counter = 0;
		}

	}
	return false;
}

void TurnRight::action()
{
	_robot->setSpeed(0, RIGHT_SPEED);
}

bool TurnRight::stopCond()
{
	double _scan = 0.0;
	double _currentYaw = 0.0;
	double _delta_yaw_plus = 0.0;
	double _delta_yaw_minus = 0.0;

	while(true)
	{
		_currentYaw = _robot->getYaw();

		if(YawToStop < 0.0)
		{
			_delta_yaw_plus = YawToStop - 0.05;
			_delta_yaw_minus = YawToStop + 0.05;
		} else {
			_delta_yaw_plus = YawToStop + 0.05;
			_delta_yaw_minus = YawToStop - 0.05;
		}

		if(_currentYaw < 0)
		{
			if(_currentYaw >= _delta_yaw_plus
					&& _currentYaw <= _delta_yaw_minus)
			{
				_robot->setSpeed(0.0, 0.0);
				return true;
			}
		}
		else
		{
			if((_currentYaw <= _delta_yaw_plus)
					&& (_currentYaw >= _delta_yaw_minus))
			{
				_robot->setSpeed(0.0, 0.0);
				return true;
			}
		}
		return false;
	}
}

TurnRight::~TurnRight() {
	// TODO Auto-generated destructor stub
}
