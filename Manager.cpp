/*
 * Manager.cpp
 *
 *  Created on: Aug 9, 2013
 *      Author: Kiril Nesenko, Nokky Goren, Tal Golan and Victor Mesika
 */

#include "Manager.h"
#include <time.h>
#include <stdlib.h>
#include "Mapping/Particle.h"
#include "SlamManager.h"
#include "time.h"
#include <stdlib.h>

Manager::Manager(char* ip, int port) :
	_robot(ip, port) {

	_arr[0] = new Forward(&_robot);
	_arr[1] = new TurnLeft(&_robot);
	_arr[2] = new TurnRight(&_robot);
	_arr[3] = new Spin(&_robot);

	_arr[0]->addNext(_arr[1]);
	_arr[0]->addNext(_arr[2]);
	_arr[0]->addNext(_arr[3]);
	_arr[1]->addNext(_arr[0]);
	_arr[2]->addNext(_arr[0]);
	_arr[3]->addNext(_arr[0]);
	_curr = _arr[0];

}
void Manager::run() {

	for(int i=0;i<20;i++)
	{
		_robot.Read();
	}

	if (_curr->startCond() == false)
	{
		cout << "[MANAGER]::[run] first loop return" << endl;
		return;
	}
	_curr->action();
	srand((unsigned)time(0));
	SlamManager* sm = new SlamManager(&_robot);
	//_robot.updateRndDeltas();
	while (_curr)
	{
		_robot.Read();

		while (_curr->stopCond() == false) {
			_robot.Read();
			_curr->action();
		}
		sm->FastUpdate();
		//_robot.updateRndDeltas();
		_curr = _curr->getNext();
	}
	cout << "[MANAGER]::[run] _curr is NULL" << endl;
}
Manager::~Manager() {
	// TODO Auto-generated destructor stub
}
