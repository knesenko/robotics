/*
 * Forward.h
 *
 *  Created on: Aug 9, 2013
 *      Author: Kiril Nesenko, Nokky Goren, Tal Golan and Victor Mesika
 */

#ifndef FORWARD_H_
#define FORWARD_H_

#include "Behavior.h"
#include "time.h"
#include <ctime>

class Forward: public Behavior {
	clock_t start, end;
public:

	Forward(Robot* robot):Behavior(robot){
	}
	void action();
	bool startCond();
	bool stopCond();
	virtual ~Forward();
};

#endif /* FORWARD_H_ */
