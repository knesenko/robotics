

#ifndef NODE_H_
#define NODE_H_


class Node
{
public:
	Node* _next;
	Particle* _particle;

	Node()
	{
		_next = NULL;
		_particle = NULL;
	}

	Node(Particle *p)
	{
		_next = NULL;
		_particle = p;
	}
};

#endif /* NODE_H_ */
