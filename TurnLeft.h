/*
 * TurnLeft.h
 *
 *  Created on: Aug 9, 2013
 *      Author: Kiril Nesenko, Nokky Goren, Tal Golan and Victor Mesika
 */

#ifndef TURNLEFT_H_
#define TURNLEFT_H_

#include "Behavior.h"

class TurnLeft: public Behavior {
	int _generateRandom();
public:
	TurnLeft(Robot* robot):Behavior(robot){}
	float YawToStop;
	int laserToStop;
	void action();
	bool startCond();
	bool stopCond();
	virtual ~TurnLeft();
};


#endif /* TURNLEFT_H_ */
