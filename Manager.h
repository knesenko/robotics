/*
 * Manager.h
 *
 *  Created on: Aug 9, 2013
 *      Author: Kiril Nesenko, Nokky Goren, Tal Golan and Victor Mesika
 */

#ifndef MANAGER_H_
#define MANAGER_H_
#include "Behavior.h"
#include "Robot.h"
#include "Forward.h"
#include "TurnLeft.h"
#include "TurnRight.h"
#include "Spin.h"

class Manager {
	Robot _robot;
	Behavior* _arr[4];
	Behavior* _curr;
public:
	Manager(char* ip, int port);
	void run();
	virtual ~Manager();
};

#endif /* MANAGER_H_ */
