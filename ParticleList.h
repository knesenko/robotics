/*
 * ParticleList.h
 *
 *  Created on: Sep 29, 2013
 *      Author: Kiril Nesenko, Nokky Goren, Tal Golan and Victor Mesika
 */

#ifndef PARTICLELIST_H_
#define PARTICLELIST_H_

#include "Mapping/Particle.h"
#include "Node.h"

class ParticleList
{
	Node* _head;
	Node* _tail;
	Node* _current;
	Node* _before_current;
	int _size;
public:
	ParticleList()
	{
		Node* _startNode = new Node();
		_head = _startNode;
		_current = _startNode;
		_tail = _startNode;
		_size = 1;
	}

	bool hasNext();
	void remove();
	void add(Particle* p);
	void sortByBelief();
	int getCount();
	Particle* getHead();
	Particle* getNext();
};

#endif /* PARTICLELIST_H_ */
