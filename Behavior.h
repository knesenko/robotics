/*
 * Behavior.h
 *
 *  Created on: Aug 9, 2013
 *      Author: Kiril Nesenko, Nokky Goren, Tal Golan and Victor Mesika
 */

#ifndef BEHAVIOR_H_
#define BEHAVIOR_H_
#include "Robot.h"

class Behavior {
	Behavior** _nextArr;
	int _size;
protected:
	Robot* _robot;
public:
	Behavior(Robot* robot): _nextArr(NULL),_size(0),
	_robot(robot){}
	virtual bool startCond()=0;
	virtual bool stopCond()=0;
	virtual void action()=0;
	bool addNext(Behavior* beh);
	Behavior* getNext();
	virtual ~Behavior();
};

#endif /* BEHAVIOR_H_ */
