/*
 * Basedefs.h
 *
 *  Created on: Aug 17, 2013
 *      Author: Kiril Nesenko, Nokky Goren, Tal Golan and Victor Mesika
 *
 *       */

#ifndef BASEDEFS_H_
#define BASEDEFS_H_

#define FORWARD_START_RAY 330
#define FORWARD_END_RAY 340
#define FORWARD_BLOCKED_DISTANCE 0.4
#define FORWARD_SPEED 0.5

#define RIGHT_START_RAY 0
#define RIGHT_END_RAY 280
#define RIGHT_BLOCKED_DISTANCE 0.4
#define RIGHT_SPEED -0.4

#define LEFT_START_RAY 386
#define LEFT_END_RAY 666
#define LEFT_BLOCKED_DISTANCE 0.4
#define LEFT_SPEED 0.4

#define RADIAN_VALUE 0.0174
#define LAZ_SPAN 240.0
#define NO_OF_PROBES 667
#define _USE_MATH_DEFINES

#define DELETE_THRESHOLD 0.3
#define DUP_THRESHOLD 0.6
#define DUP_PARTICLES 1
#define MAX_PARTICLES 100

#define MAP_SIZE 800 // 10mX10m :
#define MAP_RES 10 // resolution 5cmX5cm
#define MAP_CELL_UNKNOWN 2
#define MAP_CELL_FILLED 1
#define MAP_CELL_FREE 0

#endif /* BASEDEFS_H_ */
