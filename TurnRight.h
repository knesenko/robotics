/*
 * TurnRight.h
 *
 *  Created on: Aug 9, 2013
 *      Author: Kiril Nesenko, Nokky Goren, Tal Golan and Victor Mesika
 */

#ifndef TURNRIGHT_H_
#define TURNRIGHT_H_

#include "Behavior.h"

class TurnRight: public Behavior {
public:
	TurnRight(Robot* robot):Behavior(robot){}
	double YawToStop;
	int laserToStop;
	void action();
	bool startCond();
	bool stopCond();
	virtual ~TurnRight();
};


#endif /* TURNRIGHT_H_ */
