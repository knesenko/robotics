/*
 * Behavior.cpp
 *
 *  Created on: Aug 9, 2013
 *      Author: Kiril Nesenko, Nokky Goren, Tal Golan and Victor Mesika
 */

#include "Behavior.h"

Behavior::~Behavior() {
	// TODO Auto-generated destructor stub
	if (!_nextArr)
		delete[] _nextArr;
}
bool Behavior::addNext(Behavior* beh) {

	Behavior** tmp;
	int i;
	tmp = new Behavior*[_size + 1];
	if (!tmp)
		return false;
	for (i = 0; i < _size; i++)
		tmp[i] = _nextArr[i];
	tmp[i] = beh;
	delete[] _nextArr;
	_nextArr = tmp;
	_size++;
	return true;

}
Behavior* Behavior::getNext()
{
	int i;
	for(i=0;i<_size;i++)
	{
		if (_nextArr[i]->startCond())
		{
			return _nextArr[i];
		}
	}
	return NULL;
}
